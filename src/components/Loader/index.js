import React from 'react';

import './Loader.css';

const Loader = props => (
  <span className={`Loader ${props.modifier ? `Loader--${props.modifier}` : ''}`}>
    <span className="Loader__text">Loading {props.text} &hellip;</span>
  </span>
);

export default Loader;
