import React from 'react';

import './FlashMessage.css';

const Message = props => (
  <div className={`FlashMessage FlashMessage--${props.type}`}>
    <span>{props.message}</span>
    {props.children}
  </div>
);

export default Message;
