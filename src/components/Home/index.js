import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import JokesList from '../JokesList';
import Loader from '../Loader';
import { getNewJokes } from '../../reducers/jokes';

class Home extends Component {
  getNewJokes = (amount) => {
    this.props.getNewJokes(amount);
  };

  componentDidMount = () => {
    if (this.props.jokes.list.length === 0) {
      this.getNewJokes();
    }
  };

  render() {
    return (
      <Fragment>
        <h1>The jokes</h1>

        <button onClick={this.getNewJokes.bind(this, 10)}>Load new jokes</button>

        {this.props.jokes.error ? <h2>{this.props.jokes.error}</h2> : ''}
        {this.props.jokes.isFetching ? <Loader /> : null}
        <JokesList jokes={this.props.jokes.list} />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  jokes: state.jokes,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getNewJokes,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Home);
