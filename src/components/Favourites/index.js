import React, { Fragment, Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import JokesList from '../JokesList';
import Loader from '../Loader';
import * as api from '../../utils/api';
import { favourite } from '../../reducers/favourites';

class Favourites extends Component {
  constructor(props) {
    super(props);

    this.state = {
      randomFavouriteInterval: null,
    };
  }

  componentWillReceiveProps = (props) => {
    if (props.favourites.list.length >= 10) {
      this.stopAddRandomFavourite();
    }
  };

  toggleAddRandomFavourite = () => {
    this[
      this.state.randomFavouriteInterval ? 'stopAddRandomFavourite' : 'startAddRandomFavourite'
    ]();
  };

  stopAddRandomFavourite = () => {
    clearInterval(this.state.randomFavouriteInterval);
    this.setState({
      randomFavouriteInterval: null,
    });
  };

  startAddRandomFavourite = () => {
    this.addRandomFavourite();
    this.setState({
      randomFavouriteInterval: setInterval(this.addRandomFavourite, 1000),
    });
  };

  addRandomFavourite = () => {
    api.fetchJokes(1).then((res) => {
      this.props.favourite(res.data[0]);
    });
  };

  render() {
    return (
      <Fragment>
        <h1>Favourites</h1>
        <button
          onClick={this.toggleAddRandomFavourite}
          disabled={this.props.favourites.list.length >= 10}
        >
          {this.state.randomFavouriteInterval ? (
            <Fragment>Stop random jokes</Fragment>
          ) : (
            <Fragment>Add random jokes</Fragment>
          )}
        </button>
        {this.props.favourites.error ? <h2>{this.props.favourites.error}</h2> : ''}
        {this.props.favourites.isFetching ? (
          <Loader />
        ) : (
          <JokesList jokes={this.props.favourites.list} />
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  favourites: state.favourites,
});

const mapDispatchToProps = dispatch => bindActionCreators({ favourite }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Favourites);
