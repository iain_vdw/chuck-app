import React, { Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import FlashMessage from '../FlashMessage';
import { favourite, unfavourite } from '../../reducers/favourites';
import svg from './star.svg';
import './JokesList.css';

const JokesList = props => (
  <Fragment>
    {props.favourites.error ? <FlashMessage type="error" message={props.favourites.error} /> : null}
    {props.jokes.length ? (
      <ul className="JokesList">
        {props.jokes.map(joke => (
          <li key={joke.id} className="JokesList__item">
            <span dangerouslySetInnerHTML={{ __html: joke.joke }} className="JokesList__text" />

            {props.favourites.list.filter(fav => fav.id === joke.id).length ? (
              <button
                className="JokesList__button JokesList__button--unfavorite"
                onClick={props.unfavourite.bind(this, joke)}
              >
                <svg className="JokesList__button-icon" width="20" height="20">
                  <use xlinkHref={`${svg}#star`} />
                </svg>
              </button>
            ) : (
              <button
                className="JokesList__button JokesList__button--favorite"
                onClick={props.favourite.bind(this, joke)}
                disabled={props.favourites.list.length >= 10}
              >
                <svg className="JokesList__button-icon" width="20" height="20">
                  <use xlinkHref={`${svg}#star`} />
                </svg>
              </button>
            )}
          </li>
        ))}
      </ul>
    ) : (
      <h2>No jokes to show</h2>
    )}
  </Fragment>
);

const mapStateToProps = state => ({
  favourites: state.favourites,
});
const mapDispatchToProps = dispatch => bindActionCreators({ favourite, unfavourite }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(JokesList);
