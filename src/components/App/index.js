import React, { Fragment, Component } from 'react';
import { Route, NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Home from '../Home';
import Favourites from '../Favourites';
import LoginForm from '../LoginForm';
import Loader from '../Loader';
import FlashMessage from '../FlashMessage';
import { logout, verifyUserToken } from '../../reducers/user';
import { setupAuthHeader } from '../../utils/api';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    // Setup JWT Auth token header if user payload is present
    if (props.user.payload) {
      this.setupAuth(props.user.payload.token);
    }
  }

  componentDidMount = () => {
    // Verify token if present
    if (this.props.user.payload) {
      this.props.verifyUserToken(this.props.user.payload.token);
    }
  };

  componentWillReceiveProps = (props) => {
    // Setup JWT Auth token header if user payload is present
    if (props.user.payload) {
      this.setupAuth(props.user.payload.token);
    }
  };

  setupAuth = (token) => {
    if (token) {
      setupAuthHeader(token);
    }
  };

  render() {
    return (
      <section className="App">
        <header className="App__header">
          <span className="App__title">Chuck Norris Jokes</span>
          {this.props.user.payload ? (
            <nav className="App__nav">
              <NavLink exact className="App__nav-item" activeClassName="is-active" to="/">
                Home
              </NavLink>
              <NavLink exact className="App__nav-item" activeClassName="is-active" to="/favourites">
                Favourites
              </NavLink>
              <button className="App__nav-item" onClick={this.props.logout}>
                Log out
              </button>
            </nav>
          ) : null}
        </header>
        <main className="App__main">
          {this.props.user.payload ? (
            <Fragment>
              {this.props.user.isVerifying ? (
                <Fragment>
                  <FlashMessage message="Verifying login..." type="info">
                    <Loader />
                  </FlashMessage>
                </Fragment>
              ) : (
                <Fragment>
                  <Route exact path="/" component={Home} />
                  <Route exact path="/favourites" component={Favourites} />
                </Fragment>
              )}
            </Fragment>
          ) : (
            <Fragment>
              {setupAuthHeader(null)}
              <Route path="/" component={LoginForm} />
            </Fragment>
          )}
        </main>
      </section>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({ logout, verifyUserToken }, dispatch);

const mapStateToProps = state => ({
  routing: state.routing,
  user: state.user,
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
