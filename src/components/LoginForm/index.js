import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import './LoginForm.css';
import { login } from '../../reducers/user';
import Loader from '../Loader';
import FlashMessage from '../FlashMessage';

class LoginForm extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
    };
  }

  handleUserChange = (evt) => {
    this.setState({ username: evt.currentTarget.value });
  };

  handlePasswordChange = (evt) => {
    this.setState({ password: evt.currentTarget.value });
  };

  handleLogin = (evt) => {
    const { username, password } = this.state;

    evt.preventDefault();

    if (username && password) {
      this.props.login({ username, password });
    }
  };

  render() {
    return (
      <form onSubmit={this.handleLogin} className="LoginForm">
        <h1>Log in</h1>
        {this.props.user.error ? (
          <FlashMessage type="error" message={this.props.user.error} />
        ) : null}
        <div className="LoginForm__row">
          <label htmlFor="user">Username:</label>
          <input required id="user" onChange={this.handleUserChange} />
        </div>
        <div className="LoginForm__row">
          <label htmlFor="password">Password:</label>
          <input required id="password" type="password" onChange={this.handlePasswordChange} />
        </div>
        <button
          type="submit"
          className="LoginForm__submit"
          disabled={this.props.user.isAuthenticating}
        >
          {this.props.user.isAuthenticating ? <Loader modifier="light" /> : 'Login'}
        </button>
      </form>
    );
  }
}

const mapStateToProps = state => ({ user: state.user });

const mapDispatchToProps = dispatch => bindActionCreators({ login }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
