import axios from 'axios';

const API_ENDPOINT = 'https://localhost:8443/';
const JOKES_ENDPOINT = 'api/v1/jokes/';
const FAVOURITES_ENDPOINT = 'api/v1/users/myprofile/favourites';
const LOGIN_ENDPOINT = 'login';
const VERIFY_ENDPOINT = 'login/verify';

export const setupAuthHeader = (token) => {
  if (token) {
    axios.defaults.headers.common.Authorization = `Bearer ${token}`;
  } else {
    delete axios.defaults.headers.common.Authorization;
  }
};

// Login API calls
export const logInUser = credentials => axios.post(API_ENDPOINT + LOGIN_ENDPOINT, credentials);
export const verifyToken = token => axios.get(API_ENDPOINT + VERIFY_ENDPOINT, token);

// Get jokes API call
export const fetchJokes = amount => axios.get(API_ENDPOINT + JOKES_ENDPOINT + amount);

// Favourites API calls
export const getAllFavourites = () => axios.get(API_ENDPOINT + FAVOURITES_ENDPOINT);
export const addJokeToFavourites = joke => axios.post(API_ENDPOINT + FAVOURITES_ENDPOINT, { joke });
export const removeJokeFromFavourites = joke =>
  axios.delete(API_ENDPOINT + FAVOURITES_ENDPOINT, {
    params: {
      id: joke.id,
    },
  });
