import * as api from '../utils/api';

export const LOGOUT_REQUESTED = 'user/LOGOUT_REQUESTED';
export const LOGGED_OUT = 'user/LOGGED_OUT';
export const LOGIN_REQUESTED = 'user/LOGIN_REQUESTED';
export const LOGGED_IN = 'user/LOGGED_IN';
export const LOGIN_VERIFY_REQUESTED = 'user/LOGIN_VERIFY_REQUESTED';
export const LOGIN_VERIFY = 'user/LOGIN_VERIFY';
export const LOGIN_ERROR = 'user/LOGIN_ERROR';

const initialState = {
  isVerifying: false,
  isAuthenticating: false,
  error: null,
  payload: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    // Start user logout
    case LOGOUT_REQUESTED:
      return { ...state, isAuthenticating: true };
    // Logged out user
    case LOGGED_OUT:
      return {
        ...state,
        isAuthenticating: false,
        payload: null,
        error: null,
      };
    // Start user login
    case LOGIN_REQUESTED:
      return { ...state, isAuthenticating: true };
    // Logged in user
    case LOGGED_IN:
      return {
        ...state,
        payload: action.payload,
        isAuthenticating: false,
        error: null,
      };
    // Start verifying login token
    case LOGIN_VERIFY_REQUESTED:
      return { ...state, isVerifying: false };
    // Verified login token
    case LOGIN_VERIFY:
      return {
        ...state,
        payload: action.payload,
        isVerifying: true,
        error: null,
      };
    // Error with login
    case LOGIN_ERROR:
      return { ...state, error: action.error, isAuthenticating: false };
    default:
      return state;
  }
};

// Login action
export const login = (credentials = {}) => (dispatch) => {
  dispatch({
    type: LOGIN_REQUESTED,
  });

  api
    .logInUser(credentials)
    .then((res) => {
      dispatch({
        type: LOGGED_IN,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: LOGIN_ERROR,
        error: err.response.data.err,
      });
    });
};

// Verify action
export const verifyUserToken = token => (dispatch) => {
  dispatch({
    type: LOGIN_REQUESTED,
  });

  api.verifyToken(token).catch(() => {
    dispatch({
      type: LOGIN_ERROR,
      error: 'Login expired',
    });

    dispatch({
      type: LOGGED_OUT,
    });
  });
};

// Logout action
export const logout = () => (dispatch) => {
  dispatch({
    type: LOGOUT_REQUESTED,
  });

  dispatch({
    type: LOGGED_OUT,
  });
};
