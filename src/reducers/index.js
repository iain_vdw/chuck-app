import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import jokes from './jokes';
import user from './user';
import favourites from './favourites';

// Expose root reducer
export default combineReducers({
  routing: routerReducer,
  jokes,
  user,
  favourites,
});
