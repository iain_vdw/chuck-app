import * as api from '../utils/api';

export const JOKES_REQUESTED = 'jokes/JOKES_REQUESTED';
export const JOKES_FETCHED = 'jokes/JOKES_FETCHED';
export const JOKES_FETCH_ERROR = 'jokes/JOKES_FETCH_ERROR';

const MAX_JOKES = 10;
const initialState = {
  isFetching: false,
  error: null,
  list: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    // Start fetching jokes
    case JOKES_REQUESTED:
      return { ...state, isFetching: true, error: null };
    // Fetched jokes
    case JOKES_FETCHED:
      return {
        ...state,
        list: [...new Set([...action.list, ...state.list].map(o => JSON.stringify(o)))]
          .map(o => JSON.parse(o))
          .slice(0, MAX_JOKES),
        isFetching: false,
      };
    // Error fetching jokes
    case JOKES_FETCH_ERROR:
      return {
        ...state,
        isFetching: false,
        error: action.error,
      };
    // Fallback
    default:
      return state;
  }
};

// Fetch jokes
export const getNewJokes = (amount = 10) => (dispatch) => {
  dispatch({
    type: JOKES_REQUESTED,
  });

  api
    .fetchJokes(amount)
    .then((res) => {
      dispatch({
        type: JOKES_FETCHED,
        list: [...res.data],
      });
    })
    .catch((err) => {
      dispatch({
        type: JOKES_FETCH_ERROR,
        error: 'Error fetching jokes. Please try again.',
      });
    });
};
