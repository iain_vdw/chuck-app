import * as api from '../utils/api';

export const FAVOURITE_REQUESTED = 'favourites/FAVOURITE_REQUESTED';
export const FAVOURITED = 'favourites/FAVOURITED';
export const UNFAVOURITE_REQUESTED = 'favourites/UNFAVOURITE_REQUESTED';
export const UNFAVOURITED = 'favourites/UNFAVOURITED';
export const FAVOURITE_ERROR = 'favourites/FAVOURITE_ERROR';

const initialState = {
  list: [],
  isFetching: false,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    // Start adding favourite
    case FAVOURITE_REQUESTED:
      return { ...state, isFething: true };
    // Added favourite
    case FAVOURITED:
      return { ...state, isFetching: false, list: [...state.list, action.fav] };
    // Start removing favourite
    case UNFAVOURITE_REQUESTED:
      return { ...state, isFething: true };
    // Removed favourite
    case UNFAVOURITED:
      return {
        ...state,
        isFetching: false,
        list: [...state.list].filter(f => f.id !== action.fav.id),
      };
    // Error when (un)favouriting
    case FAVOURITE_ERROR:
      return {
        ...state,
        isFetching: false,
        error: action.message,
      };
    // Fallbacks
    default:
      return state;
  }
};

// Favourite action
export const favourite = fav => (dispatch) => {
  dispatch({
    type: FAVOURITE_REQUESTED,
  });

  api
    .addJokeToFavourites(fav)
    .then(() => {
      dispatch({
        type: FAVOURITED,
        fav,
      });
    })
    .catch((err) => {
      dispatch({
        type: FAVOURITE_ERROR,
        message: err.response.data.msg,
      });
    });
};

// Unfavourite action
export const unfavourite = fav => (dispatch) => {
  dispatch({
    type: UNFAVOURITE_REQUESTED,
  });

  api
    .removeJokeFromFavourites(fav)
    .then(() => {
      dispatch({
        type: UNFAVOURITED,
        fav,
      });
    })
    .catch((err) => {
      dispatch({
        type: FAVOURITE_ERROR,
        message: err.response.data.msg,
      });
    });
};
