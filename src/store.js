import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import persistState from 'redux-localstorage';

import rootReducer from './reducers';

export const history = createHistory();

// Inital state of the app
const initialState = {};

// Redux enhances
const enhancers = [persistState()];

// Redux middleware
const middleware = [thunk, routerMiddleware(history)];

// Add Redux devtools extensions as enhancer in dev mode
if (process.env.NODE_ENV === 'development') {
  const { devToolsExtension } = window;

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

// Compose Redux enhances into single enhancer functions
const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);

// Create the Redux store
const store = createStore(rootReducer, initialState, composedEnhancers);

export default store;
